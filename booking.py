from trytond.model import ModelView, fields
from trytond.pool import Pool, PoolMeta
from trytond.wizard import Button, StateTransition, StateView, Wizard

from .wubook_api import WubookAPI


class Booking(metaclass=PoolMeta):
    __name__ = 'hotel.booking'

    @classmethod
    def get_wubook_bookings(cls, rcode=None):
        pool = Pool()
        Configuration = pool.get('hotel.configuration')
        Shop = pool.get('sale.shop')
        APILog = pool.get('api.log')
        config = Configuration.get_configuration()
        shops = Shop.search([
            ('wubook_code', '!=', None),
        ])
        if not config.wubook_token or not shops:
            print("Al parecer no se ha configurado en el hotel el token de wubook!!!")
            return

        # FIXME: must search specific wubook code property
        shop = shops[0]
        wapi = WubookAPI(config.wubook_token, shop.wubook_code)
        # Log.create
        if rcode:
            # Just catch one reservation
            res, info = wapi.get_booking(rcode)
        else:
            # Get all new reservations
            res, info = wapi.get_all_booking(test=True)

        if res == 0:
            to_create, to_cancel = APILog.add_log_wubook(info, shop.wubook_code)
            try:
                APILog.process(to_create)
            except:
                print("Fallo el procesar logs")

            # try:
            #     if to_cancel:
            #         cls.cancel(to_cancel)
            # except:
            #     print("Fallo el procesar logs cancelados")

    def get_data_card(self):
        pool = Pool()
        Shop = pool.get('sale.shop')
        Configuration = pool.get('hotel.configuration')
        config = Configuration.get_configuration()
        shops = Shop.search([
            ('wubook_code', '!=', None),
        ])
        if not config.wubook_token or not shops or not self.reservation_code:
            return super(Booking, self).get_data_card()
        shop = shops[0]
        wapi = WubookAPI(config.wubook_token, shop.wubook_code)
        res, info = wapi.get_ccard_info(self.reservation_code, config.wubook_cc)
        if res == 0:
            return info
        else:
            print(info)
            print('error get data card')


class WubookBookingSynchronizeStart(ModelView):
    "Wubook Booking Synchronize Start"
    __name__ = 'hotel_wubook.synchronize.start'
    start_date = fields.Date('Start Date')
    end_date = fields.Date('End Date')
    reservation_code = fields.Char('Reservation Code')


class WubookBookingSynchronize(Wizard):
    "Wubook Booking Synchronize"
    __name__ = "hotel_wubook.synchronize"
    start = StateView(
        'hotel_wubook.synchronize.start',
        'hotel_wubook.synchronize_booking_view_form', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('OK', 'accept', 'tryton-ok', default=True),
        ])
    accept = StateTransition()

    def transition_accept(self):
        Booking = Pool().get('hotel.booking')
        code = self.start.reservation_code
        if code:
            code = int(code)
            Booking.get_wubook_bookings(code)

        return 'end'
