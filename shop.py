# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.

from trytond.model import fields
from trytond.pool import PoolMeta


class Shop(metaclass=PoolMeta):
    __name__ = 'sale.shop'
    wubook_code = fields.Char('Wubook Code')
    wubook_token = fields.Char('Wubook Token')
