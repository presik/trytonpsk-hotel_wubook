# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.

from datetime import date, datetime, timedelta

from trytond.pool import Pool, PoolMeta

from .wubook_api import WubookAPI

FMT_DATE = "%Y-%m-%d"


def to_wdate(_date):
    return _date.strftime("%d/%m/%Y")


class FolioOccupancy(metaclass=PoolMeta):
    __name__ = 'hotel.folio.occupancy'

    @classmethod
    def update_availability(cls, dates, kind=None):
        super().update_availability(dates, kind)
        pool = Pool()
        Room = pool.get('hotel.room')
        Folio = pool.get('hotel.folio')
        start_date = min(dates)
        end_date = max(dates)
        _start_date = datetime.strptime(start_date, FMT_DATE).date()
        _end_date = datetime.strptime(end_date, FMT_DATE).date() + timedelta(days=1)
        end_date = str(_end_date)
        availability = Room.get_availability(start_date, end_date)
        print(" availability => ", availability)
        Folio.update_wubook_availability(_start_date, availability)


class Folio(metaclass=PoolMeta):
    __name__ = 'hotel.folio'

    @classmethod
    def update_wubook_availability(
            cls, start_date, availability, products_ids=None):
        pool = Pool()
        Configuration = pool.get('hotel.configuration')
        Product = pool.get('product.product')
        config = Configuration.get_configuration()
        if not config.wubook_code:
            return

        domain = [
            ('channel_code', '!=', None),
            ('department', '=', 'accommodation'),
        ]
        if products_ids:
            domain.append(('id', 'in', products_ids))

        products = Product.search(domain)
        codes = {pdt.id: pdt.channel_code for pdt in products}

        # codesTest = {pdt.id: pdt.template.name for pdt in products}
        start_date = to_wdate(start_date)
        rooms = []
        for acco_id, data in availability.items():
            # For testing purposes
            # print("name........", codesTest[acco_id])
            # print("dates...............................................")
            # for x, k in dates.items():
            #     print("Fecha ...", x, k)
            # print("...............................................")
            days = []
            print('dates....', acco_id, data)
            for dt, avail in data['dates'].items():
                days.append({'avail': avail})
            if not codes.get(acco_id):
                continue
            rooms.append({
                'id': codes[acco_id],
                'days': days,
            })
        mode = 'production'
        if mode == 'development':
            print("MODO DESARROLLO LOS DATOS NO SE ENVIAN...................", rooms)
            return

        print("!!!MODO PRODUCCION DATOS ENVIADOS A WUBOOK...................", start_date)
        wapi = WubookAPI(config.wubook_token, config.wubook_code)
        print(rooms)
        print('---------------------------------')
        res = wapi.update_availability(start_date, rooms)
        print("Response....", res)

    @classmethod
    def sync_wubook_availability(cls, start_date=None, end_date=None, rcode=None):
        pool = Pool()
        Room = pool.get('hotel.room')
        start_date = date.today()
        end_date = start_date + timedelta(days=180)
        availability = Room.get_availability(start_date, end_date)
        # print("availability ....", availability)
        cls.update_wubook_availability(start_date, availability)
