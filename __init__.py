# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.

from trytond.pool import Pool
from . import configuration
from . import booking
from . import folio
from . import channel
from . import product
from . import log
from . import ir
from . import shop


def register():
    Pool.register(
        channel.Channel,
        configuration.Configuration,
        booking.Booking,
        booking.WubookBookingSynchronizeStart,
        log.Log,
        ir.Cron,
        folio.Folio,
        folio.FolioOccupancy,
        product.SyncChannelStart,
        shop.Shop,
        module='hotel_wubook', type_='model')
    Pool.register(
        booking.WubookBookingSynchronize,
        product.SyncChannel,
        module='hotel_wubook', type_='wizard')
