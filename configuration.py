# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import fields
from trytond.pool import PoolMeta


class Configuration(metaclass=PoolMeta):
    __name__ = 'hotel.configuration'
    wubook_code = fields.Char('Wubook Code', required=True)
    wubook_token = fields.Char('Wubook Token', required=True)
    wubook_cc = fields.Char('Wubook Code CC', required=True)
