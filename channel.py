# This file is part of Presik.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import fields
from trytond.pool import PoolMeta


class Channel(metaclass=PoolMeta):
    __name__ = 'hotel.channel'
    wubook_code = fields.Char('Wubook Code')
