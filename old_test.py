import requests
import json

uspw = ('wb_d2f10d96-2d61-11e9-a19d-001a4ae7b219', None)
data = {'dfrom': '20/02/2020', 'dto': '28/02/2020'}
response = requests.post(
    'https://kapi.wubook.net/kapi/revenue/get_room_reservations', data, auth=uspw)
res = json.loads(response.text)

codigos = []

for booking in res['data']:
    codigos.append(booking['code'])

for codigo in codigos:
    data = {'rcode': codigo}
    response = requests.post(
        'https://kapi.wubook.net/kapi/rsrvs/reservation', data, auth=uspw)
    print('----------------------------------------------')
    print(response.text)
