''' Wubook Authentication functions for Webservices '''
import xmlrpc.client

from env import url_api, lcode, token


def printfmt(bks):
    for bk in bks:
        print("======================================================")
        for key, value in bk.items():
            print(type(value), " => " + key + " : ",  value)


def get_rooms():
    server = xmlrpc.client.Server(url_api)
    _, rooms = server.fetch_rooms(token, lcode)
    for room in rooms:
        print("======================================================")
        for key, value in room.items():
            print(" => " + key + " : ",  value)


def fetch_single_room(rid):
    server = xmlrpc.client.Server(url_api)
    res, room = server.fetch_single_room(token, lcode, rid)

    # for room in rooms:
    print("======================================================")
    print(res)
    print(room)
    # for key, value in room.items():
    #     print(" => " + key + " : ",  value)


def add_room(name, beds, defprice, avail, shortname):
    server = xmlrpc.client.Server(url_api)
    woodoo = 0
    res, info = server.new_room(token, lcode, woodoo, name, beds,
        defprice, avail, shortname)
    print("Response....", res)
    print("Info....", info)


def get_booking(rcode):
    server = xmlrpc.client.Server(url_api)
    res, info = server.fetch_booking(token, lcode, rcode)
    # print("res ....", res, info)
    printfmt(info)


def get_all_booking():
    server = xmlrpc.client.Server(url_api)
    res, info = server.fetch_new_bookings(token, lcode)
    printfmt(info)


def set_availability():
    server = xmlrpc.client.Server(url_api)
    start_date = '15/11/2023'
    rooms = [
        {'id': 621087, 'days': [{'avail': 3, 'no_ota': 1}]},
        {'id': 621084, 'days': [{'avail': 7}]},
    ]
    res, info = server.update_avail(token, lcode, start_date, rooms)
    print("Response....", res, info)

# add_pricing_plan()

# set_availability()

rcode = '1726086411'
get_booking(rcode)

# get_all_booking()

# id_ = '1076941104'

# fetch_single_room(id_)
# get_rooms()
