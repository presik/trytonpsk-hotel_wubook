# This file is part of Presik.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import fields
from trytond.wizard import Wizard, StateView, StateTransition, Button
from trytond.model import ModelView
from trytond.pool import Pool
from .wubook_api import WubookAPI
from trytond.transaction import Transaction


class SyncChannelStart(ModelView):
    "Synchronize Channel Start"
    __name__ = 'hotel_wubook.product_synchronize.start'
    wubook_room = fields.Selection('get_rooms', 'Wubook Room', required=True)

    def get_rooms():
        Configuration = Pool().get('hotel.configuration')
        config = Configuration.get_configuration()
        if not config.wubook_code:
            return

        wapi = WubookAPI(config.wubook_token, config.wubook_code)
        rooms = wapi.get_rooms()
        res = []
        for room in rooms:
            res.append((str(room['id']), room['name']))
        return res


class SyncChannel(Wizard):
    "Synchronize Channel"
    __name__ = 'hotel_wubook.product_synchronize'
    start = StateView(
        'hotel_wubook.product_synchronize.start',
        'hotel_wubook.product_synchronize_view_form', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('OK', 'accept', 'tryton-ok', default=True),
        ])
    accept = StateTransition()

    @classmethod
    def __setup__(cls):
        super(SyncChannel, cls).__setup__()

    def transition_accept(self):
        Template = Pool().get('product.template')
        active_id = Transaction().context['active_id']
        template = Template(active_id)
        Template.write([template], {'channel_code': self.start.wubook_room})
        return 'end'
