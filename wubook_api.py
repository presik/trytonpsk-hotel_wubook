
import xmlrpc.client

URL_API = 'https://wired.wubook.net/xrws/'


def printfmt(data):
    for bk in data:
        print("======================================================")
        for key, value in bk.items():
            print(type(value), " => " + key + " : ", value)


class WubookAPI:
    """
    Get Wubook interface and request data for Tryton
    """

    def __init__(self, token, lcode):
        print('Connecting to Wubook API.....')
        self.server = xmlrpc.client.Server(URL_API)
        self.token = token
        self.lcode = lcode

    def get_all_booking(self, test=False):
        res, info = self.server.fetch_new_bookings(self.token, self.lcode)
        return res, info

    def get_booking(self, rcode):
        res, info = self.server.fetch_booking(self.token, self.lcode, rcode)
        return res, info

    def add_room(self, name, beds, defprice, avail, shortname):
        woodoo = 0
        res, info = self.server.new_room(
                self.token, self.lcode, woodoo, name, beds,
                defprice, avail, shortname)
        return res, info

    def get_rooms(self):
        res, info = self.server.fetch_rooms(self.token, self.lcode)
        print("info....", info)
        return info

    def update_availability(self, start_date, rooms):
        # Update availability in Wubook
        # start_date = '15/11/2023'
        # rooms = [
        #     {'id': 621087, 'days': [{'avail': 3, 'no_ota': 1}]},
        #     {'id': 621084, 'days': [{'avail': 7}]},
        # ]
        res, info = self.server.update_avail(
            self.token, self.lcode, start_date, rooms)
        print("Response....", res, info)

    def get_ccard_info(self, rcode, code_validate):
        res, info = self.server.fetch_ccard(self.token, self.lcode, rcode, code_validate)
        return res, info
