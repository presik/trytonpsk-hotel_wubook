import json
from datetime import datetime, timedelta
from decimal import Decimal

import orjson
from trytond.exceptions import UserError
from trytond.i18n import gettext
from trytond.pool import Pool, PoolMeta

WB_CHANNELS_CODE = {
    1: 'Expedia',
    2: 'Booking.com',
    43: 'Airbnb',
    0: 'Despegar',
}

ROUND_TWO = Decimal('0.01')


def to_date(_date):
    return datetime.strptime(_date, "%d/%m/%Y").date()


def to_datetime(_date):
    return datetime.strptime(_date, "%d/%m/%Y %H:%M:%S")


class Log(metaclass=PoolMeta):
    __name__ = 'api.log'

    def process_wubook(self):
        self.add_booking_wubook()

    def add_booking_wubook(self, shop=None):
        pool = Pool()
        Booking = pool.get('hotel.booking')
        # FIXME WE NEED LIMIT THIS SEARCH
        fields_names = ['ota_booking_code', 'reservation_code', 'number']
        res = Booking.search_read([
            ('ota_booking_code', '!=', None),
            ], fields_names=fields_names)
        ota_codes = [bk['ota_booking_code'] for bk in res]
        res_wb_number = {bk['reservation_code']: bk for bk in res}
        ota_codes_bk = {bk['ota_booking_code']: bk for bk in res}
        data = orjson.loads(self.request_json)
        channel_code = data['channel_reservation_code']
        reservation_code = str(data['reservation_code'])
        id_channel = data.get('id_channel', '')
        _channel_name = WB_CHANNELS_CODE.get(id_channel, 'Unknown')
        if self.status in ('error', 'pending') and channel_code and channel_code in ota_codes:
            self.status = 'created'
            # FIXME: key must be a tuple (channel, channel_reservation_code)
            # FIXME: Add raise user error if reservation not found
            _booking = ota_codes_bk[channel_code]
            # FIXME: if reservation_code != prev_reservation_code => update booking
            number = _booking['number']
            if not number:
                rec = Booking(_booking['id'])
                Booking.set_number([rec])
                number = rec.number
            self.number = number
            self.description = _channel_name
            self.msg_response = 'success'
            self.save()
            return

        if reservation_code in res_wb_number:
            return

        try:
            new_booking = Log.get_wubook_booking(data, shop, self.ignore_overbooking)
            booking = Booking(new_booking)
            self.status = 'created'
            self.number = booking.number
            self.msg_response = 'success'
            self.save()
            Booking.confirm([booking])
        except Exception as e:
            print("Exception...", e)
            self.status = 'error'
            self.msg_response = str(e)
            self.save()

    @classmethod
    def add_log_wubook(cls, values, property_code=None):
        now = datetime.now() - timedelta(days=120)
        pool = Pool()
        Booking = pool.get('hotel.booking')
        logs = cls.search_read([
            ('create_date', '>=', now),
            ('model_name', '=', 'hotel.booking'),
            ('application', '=', 'wubook'),
        ], fields_names=['reference'])
        current_logs = [log['reference'] for log in logs]
        bk_to_create = []
        cancelled = []
        for wbk in values:
            res_code = str(wbk['reservation_code'])
            if res_code in current_logs:
                # Check if the reservation is cancelled
                if wbk['status'] == 5:
                    print("Encontre un reserva cancelada....")
                    cancelled.append(res_code)
                continue
            wbk['property_code'] = property_code
            date_received = to_datetime(wbk['date_received_time'])
            msg = None
            new_log, = cls.create([{
                'endpoint': '',
                'reference': res_code,
                'record_date': date_received.date(),
                'msg_response': msg,
                'request_json': json.dumps(wbk),
                'status': 'pending',
                'model_name': 'hotel.booking',
                'application': 'wubook',
            }])
            bk_to_create.append(cls(new_log))

        bk_to_cancel = []
        # if cancelled:
        #     log_to_cancel = cls.search([
        #         ('reference', 'in', cancelled),
        #         ('model_name', '=', 'hotel.booking'),
        #         ('application', '=', 'wubook'),
        #     ])
        #     cls.write(log_to_cancel, {'status': 'cancelled'})
        #     bk_to_cancel = Booking.search([
        #         ('reservation_code', 'in', cancelled)
        #     ])
        return bk_to_create, bk_to_cancel

    @classmethod
    def get_wubook_booking(cls, wbk, shop=None, overbooking=False):
        pool = Pool()
        Channel = pool.get('hotel.channel')
        Booking = pool.get('hotel.booking')
        Country = pool.get('country.country')
        Product = pool.get('product.product')
        Shop = pool.get('sale.shop')
        if not shop:
            # FIXME : must search by specific wubook_code
            shops = Shop.search([
                ('wubook_code', '!=', None),
            ])
            if not shops:
                return

            shop = shops[0]

        first_name = wbk['customer_name'].upper()
        first_family_name = wbk['customer_surname'].upper()
        contact = first_name + ' ' + first_family_name
        date_received = to_datetime(wbk['date_received_time'])
        arrival_date = to_date(wbk['date_arrival'])
        departure_date = to_date(wbk['date_departure'])
        reservation_code = str(wbk['reservation_code'])
        ota_code = str(wbk['channel_reservation_code'])
        channel_id = None
        rate_plan_id = None
        price_list_id = None

        mobile = wbk.get('customer_phone', None) or '0000000'
        email = wbk.get('customer_mail', 'example@gmail.com').lower()
        country_code = wbk.get('customer_country', None)
        address = wbk.get('customer_address', '').upper()
        type_document = '13'
        if country_code != 'CO':
            type_document = '41'  # Passport
        try:
            country, = Country.search([
                ('code', '=', country_code),
            ])
        except:
            country, = Country.search([
                ('code', '=', 'CO'),
            ])
        data = {
            'contact': contact,
            'booking_date': date_received,
            'created_channel': date_received,
            'ota_booking_code': ota_code,
            'bill_to': 'holder',
            'media': 'ota',
            'channel_manager': 'wubook',
            'reservation_code': reservation_code,
            'environment': shop.id,
            'notes': wbk['customer_notes'],
        }
        folios = []
        ignore_rooms = []
        for booked_room in wbk['booked_rooms']:
            try:
                pdt, = Product.search([
                    ('channel_code', '=', str(booked_room['room_id'])),
                ])
            except:
                raise UserError(gettext('hotel.msg_accommodation_not_found'))
            room_id = Booking.get_random_room(
                pdt, arrival_date, departure_date, overbooking, ignore_rooms)
            ignore_rooms.append(room_id)
            occupancy = []
            price = 0
            for room_day in booked_room['roomdays']:
                # wu_rate_id = str(room_day['ancillary'].get("channel_rate_id", ""))
                price = Decimal(room_day['price']).quantize(ROUND_TWO)
                occupancy.append({
                    'occupancy_date': to_date(room_day['day']),
                    'unit_price': price,
                    'state': 'draft',
                })
            guests = [{
                'name': contact,
                'type_document': type_document,
                'email': email,
                'mobile': mobile,
                'address': address,
                'sex': 'male',
                'type_guest': 'adult',
                'country': country.id,
                'first_name': first_name,
                'first_family_name': first_family_name,
            }]
            num_adults = booked_room.get('guests_adults', 1)
            num_children = booked_room.get('guests_children', 0)
            pax = num_adults + num_children
            folios.append({
                'product': pdt.id,
                'room': room_id,
                'arrival_date': arrival_date,
                'departure_date': departure_date,
                'unit_price': Decimal(price),
                'num_adults': num_adults,
                'num_children': num_children,
                'pax': pax,
                'occupancy': [('create', occupancy)],
                'guests': [('create', guests)],
            })

        wu_channel_id = str(wbk['id_channel'])
        if wu_channel_id:
            channels = Channel.search([
                ('wubook_code', '=', wu_channel_id),
            ])
            if channels:
                channel = channels[0]
                channel_id = channel.id
                if channel.rate_plan:
                    rate_plan_id = channel.rate_plan.id
                    if not price_list_id:
                        price_list_id = channel.rate_plan.price_lists[0].id

        data['channel'] = channel_id
        data['price_list'] = price_list_id
        data['rate_plan'] = rate_plan_id
        data['lines'] = [('create', folios)]
        new_booking, = Booking.create([data])
        return new_booking
